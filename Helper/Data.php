<?php

namespace GDW\Core\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{

	const BASE_PATH_COLOR = 'gdwcore/';

	public function getConfigValue($field, $storeId = null)
	{
		return $this->scopeConfig->getValue(
			$field, ScopeInterface::SCOPE_STORE, $storeId
		);
	}

	public function getVal($group,$code, $storeId = null)
	{
		return $this->getConfigValue(self::BASE_PATH_COLOR.$group.'/'. $code, $storeId);
	}


	public function getAdminUrl()
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		return $objectManager->create('Magento\Backend\Helper\Data')->getHomePageUrl();
	}

	public function getGlobalInfoModule(){
		$html  = '<strong>Autor: </strong> José Cruz Chávez <br/>';
		$html .= '<strong>Donaciones: </strong> <a href="https://www.paypal.me/gestiondigitalweb" target="_blank">Mediante Paypal</a><br/>';
		$html .= '<strong>Sitio Web: </strong> <a href="https://gestiondigitalweb.com" target="_blank">https://gestiondigitalweb.com</a><br/>';
		$html .= '<strong>Dudas o requerimientos: </strong> <a href="mailto:soporte@gestiondigitalweb.com" target="_blank">soporte@gestiondigitalweb.com</a><br/>';
		return $html; 		
	}

	public function getInsignea(){
		return '<img src="https://gestiondigitalweb.com/wp-content/uploads/2016/12/logotipo_gestiondigitalweb.png" style="display:block; margin:auto; width:100%; max-width:50px;" />';
	}

	public function getInfo($name,$version,$desc){
		$insignea = $this->getInsignea();
		$globalInfo = $this->getGlobalInfoModule();

$html = <<<HTML
    <table style="background:#f8f8f8; border:1px solid #ccc; min-height:100px; margin:5px 0; padding:15px; width:100%;"><tr>
    <td valign="middle" style="width:100px; padding:8px;">$insignea</td>
    <td valign="top" style="width:40%; padding:8px;">
        <p><strong>Info. de este Módulo:</strong></p>
        <p>
            <strong>Nombre:</strong> $name <br/>
            <strong>Versión:</strong> $version <br/>
            <strong>Descripción:</strong> $desc <br/>
        </p>
    </td>
    <td valign="top" style="width:45%; padding:8px;"><p><strong>Extensiones y tiendas online Magento</strong></p><p>$globalInfo</p></td>
    </tr></table>
HTML;
	return $html;
	}


	public function getInfoSimple($name,$version,$descFull = null, $linkconfig = null){
		$html  = '<table style="background:#f8f8f8; border:0px solid #ccc; margin:0px !important; padding:15px; width:100%;"><tr>';
        $html .= '<td style="padding:8px; width:33%;"><strong>Nombre: </strong>'.$name.'</td>';
        $html .= '<td style="padding:8px; width:33%;"><strong>Versión: </strong>'.$version.'</td>';
		  if($linkconfig != null){
			$html .= '<td style="padding:8px; width:33%;"><strong><a href="'.$linkconfig.'">Configurar</a></strong></td>';
		  }else{
			$html .= '<td style="padding:8px; width:33%;">&nbsp;</td>';
		  }
    	$html .= '</tr></table>';
		  if($descFull != null){
			$html .= '<div style="background:#f8f8f8; border:0px solid #ccc; margin:0px !important; padding:8px;">';
			$html .= $descFull;
			$html .= '</div>';
		  }
	  return $html;
	}
}