<?php
namespace GDW\Core\Block\Adminhtml\System\Core;

use GDW\Core\Helper\Data;
use Magento\Framework\Module\ModuleListInterface;

class ModuleInfo extends \Magento\Config\Block\System\Config\Form\Fieldset
{
    const MODULE_NAME = 'GDW_Core';
    
    public function __construct( Data $helpData, ModuleListInterface $moduleList) {
        $this->helpData = $helpData;
        $this->_moduleList = $moduleList;
    }

    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $name = self::MODULE_NAME;
        $version = $this->getVersion();
        $desc = $this->getDesc();

        return $this->helpData->getInfo($name,$version,$desc);
    }

    public function getVersion()
    {
        return $this->_moduleList->getOne(self::MODULE_NAME)['setup_version'];
    }

    public function getDesc()
    {
        return 'Necesario para las funciones generales.';
    }
}