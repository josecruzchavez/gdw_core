<?php
namespace GDW\Core\Block\Adminhtml\System\Core;

use GDW\Core\Helper\Data;
use Magento\Framework\Module\ModuleListInterface;

class ModuleInfoSimple extends \Magento\Config\Block\System\Config\Form\Fieldset
{
    const MODULE_NAME = 'GDW_Core';
    
    protected $moduleList;

    public function __construct( Data $helpData, ModuleListInterface $moduleList) {
        $this->helpData = $helpData;
        $this->_moduleList = $moduleList;
    }

    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $name = self::MODULE_NAME;
        $version = $this->getVersion();
        $desc = $this->getDescFull();

        return $this->helpData->getInfoSimple($name, $version, $desc);
    }

    public function getVersion()
    {
        return $this->_moduleList->getOne(self::MODULE_NAME)['setup_version'];
    }

    public function getDesc()
    {
        return 'Necesario para las funciones generales.';
    }

    public function getDescFull()
    {
        $html = 'Este módulo contiene configuraciones básicas pero necesarias para cualquier otro módulo creado por GDW.';
        return $html;
    }

}